package com.ninetynine.companyapi.company.infrastructure

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
class GetCompanyControllerIT {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var getCompanyController: GetCompanyController

    @Test
    fun `when send GET request then get all companies`(){
        mockMvc.
        perform(MockMvcRequestBuilders.get("/companies")).
        andExpect(MockMvcResultMatchers.status().isOk)
        //TODO expect a list of values
    /*.
        andExpect(
            MockMvcResultMatchers.content().string("[{},{},{},{}]")
        )*/
    }

    //TODO test for /companies/{id}/shares?from=?&to=?
}