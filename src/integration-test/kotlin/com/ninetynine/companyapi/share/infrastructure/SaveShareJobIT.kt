package com.ninetynine.companyapi.share.infrastructure

import com.ninetynine.companyapi.share.infrastructure.repositories.HttpShareRepository
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class SaveShareJobIT {

    @MockkBean
    lateinit var repository: HttpShareRepository

    @Test
    fun `when dont get some shares then dont call to save usecase`(){
        every { repository.findAll() } returns listOf()
        Thread.sleep(1000)

        //verify(exactly = 0) { SaveShareCmd().execute(any()) }
    }

    @Test
    fun `when get some share then save them`(){
        every { repository.findAll() } returns listOf()
        Thread.sleep(10000)

        verify { repository.findAll() }
        //verify { saveShareCmd.execute(SHARE) }
    }
}