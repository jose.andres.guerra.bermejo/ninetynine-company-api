package com.ninetynine.companyapi.share.infrastructure.repositories

import com.ninetynine.companyapi.company.domain.Company
import com.ninetynine.companyapi.share.domain.Share
import com.ninetynine.companyapi.share.domain.ShareFilter
import com.ninjasquad.springmockk.SpykBean
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.test.context.ActiveProfiles
import java.time.Instant
import java.util.*

@SpringBootTest
@ActiveProfiles("test")
class JpaShareRepositoryIT {

    @Autowired
    @SpykBean
    lateinit var repository: JpaShareRepository

    @Test
    fun `when save a share with a random company then get an error`(){
        assertThrows<DataIntegrityViolationException> {
            repository.save(Share(null, 1.0, Date.from(Instant.now()), Company(1000, "test")))
        }
    }

    @Test
    fun `when send a filter then get filtered shares`(){
        Thread.sleep(1000)
        val sut = repository.findByFilter(ShareFilter(companyId = 1, Date.from(Instant.now()), Date.from(Instant.now())))

        assertTrue(sut.isNotEmpty())
    }

}