-- liquibase formatted sql

-- changeset liquibase:1
CREATE TABLE share (id INT NOT NULL AUTO_INCREMENT, price DOUBLE, date DATETIME, company_id INT, PRIMARY KEY (id), FOREIGN KEY(company_id) REFERENCES company(id));