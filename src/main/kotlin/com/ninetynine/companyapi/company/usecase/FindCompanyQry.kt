package com.ninetynine.companyapi.company.usecase

import com.ninetynine.companyapi.company.domain.Company
import com.ninetynine.companyapi.company.domain.CompanyRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

class FindCompanyQry(private val repository: CompanyRepository) {

    fun execute(): List<Company> {
        return repository.findAll()
    }
}