package com.ninetynine.companyapi.company.usecase

import com.ninetynine.companyapi.company.domain.Company
import com.ninetynine.companyapi.company.domain.CompanyRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

class SaveCompanyCmd(@Autowired private val repository: CompanyRepository) {

    fun execute(company: Company){
        repository.save(company)
    }
}