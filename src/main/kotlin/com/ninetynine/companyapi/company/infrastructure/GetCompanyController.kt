package com.ninetynine.companyapi.company.infrastructure

import com.ninetynine.companyapi.company.infrastructure.dtos.CompanyDto
import com.ninetynine.companyapi.company.usecase.FindCompanyQry
import com.ninetynine.companyapi.company.infrastructure.mappers.CompanyMapper
import com.ninetynine.companyapi.company.domain.CompanyRepository
import com.ninetynine.companyapi.share.domain.Share
import com.ninetynine.companyapi.share.domain.ShareFilter
import com.ninetynine.companyapi.share.domain.ShareRepository
import com.ninetynine.companyapi.share.usecase.FindByFilterShareQry
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController
import java.util.Date

@RestController
@RequestMapping("/companies")
class GetCompanyController(
    private val mapper: CompanyMapper,
    private val repository: CompanyRepository,
    private val shareRepository: ShareRepository
) {

    @GetMapping
    @ResponseBody
    fun get(): ResponseEntity<List<CompanyDto>>{
        return ResponseEntity.ok(mapper.companyListToDto(FindCompanyQry(repository).execute()))
    }

    @GetMapping("/{id}/shares")
    @ResponseBody
    fun getSharesByFilter(@PathVariable id: Long, @RequestParam from: Long, @RequestParam to: Long): ResponseEntity<List<Share>> {
        return ResponseEntity.ok(FindByFilterShareQry(shareRepository).execute(ShareFilter(id, Date(from), Date(to))))
    }
}