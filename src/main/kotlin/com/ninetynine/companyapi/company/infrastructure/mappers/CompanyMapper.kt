package com.ninetynine.companyapi.company.infrastructure.mappers

import com.ninetynine.companyapi.company.infrastructure.dtos.CompanyDto
import com.ninetynine.companyapi.company.domain.Company
import org.springframework.stereotype.Service

@Service
class CompanyMapper {

    fun companyListToDto(companies: List<Company>): List<CompanyDto> {
        return companies.map { toDto(it) }
    }

    fun toDto(company: Company): CompanyDto {
        return CompanyDto(
            company.id!!,
            company.name
        )
    }

}
