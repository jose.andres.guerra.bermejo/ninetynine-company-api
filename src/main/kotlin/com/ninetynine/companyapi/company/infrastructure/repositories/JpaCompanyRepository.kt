package com.ninetynine.companyapi.company.infrastructure.repositories

import com.ninetynine.companyapi.company.domain.Company
import com.ninetynine.companyapi.company.domain.CompanyRepository
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface JpaCompanyRepository: CompanyRepository, JpaRepository<Company, Long>