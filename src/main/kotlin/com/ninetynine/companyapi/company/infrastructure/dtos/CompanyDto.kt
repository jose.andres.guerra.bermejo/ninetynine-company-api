package com.ninetynine.companyapi.company.infrastructure.dtos

data class CompanyDto(
    val id: Long,
    val name: String
)
