package com.ninetynine.companyapi.company.domain

import jakarta.persistence.*

@Entity
@Table
data class Company(
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    val id: Long? = null,
    val name: String
)