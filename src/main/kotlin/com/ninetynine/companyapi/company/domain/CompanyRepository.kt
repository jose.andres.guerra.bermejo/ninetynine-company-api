package com.ninetynine.companyapi.company.domain

interface CompanyRepository {

    fun findAll(): List<Company>
    fun save(company: Company)
}