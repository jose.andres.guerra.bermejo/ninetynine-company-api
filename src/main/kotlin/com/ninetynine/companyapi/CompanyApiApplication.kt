package com.ninetynine.companyapi

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
@EnableScheduling
class CompanyApiApplication

fun main(args: Array<String>) {
	runApplication<CompanyApiApplication>(*args)
}
