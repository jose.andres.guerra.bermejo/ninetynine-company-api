package com.ninetynine.companyapi.share.domain

import com.ninetynine.companyapi.company.domain.Company
import jakarta.persistence.*
import java.util.Date

@Entity
@Table
data class Share(
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    val id: Long? = null,
    val price: Double,
    val date: Date,
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "company_id")
    val company: Company
)