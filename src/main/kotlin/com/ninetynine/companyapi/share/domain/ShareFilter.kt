package com.ninetynine.companyapi.share.domain

import java.util.Date

data class ShareFilter(
    val companyId: Long,
    val from: Date,
    val to: Date
)
