package com.ninetynine.companyapi.share.domain

interface ShareRepository {
    fun save(share: Share)
    fun findAll(): List<Share>
    fun findByFilter(filter: ShareFilter): List<Share>
}