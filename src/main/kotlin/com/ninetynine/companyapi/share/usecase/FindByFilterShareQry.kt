package com.ninetynine.companyapi.share.usecase

import com.ninetynine.companyapi.share.domain.Share
import com.ninetynine.companyapi.share.domain.ShareFilter
import com.ninetynine.companyapi.share.domain.ShareRepository
import org.springframework.stereotype.Service

class FindByFilterShareQry(private val repository: ShareRepository) {

    fun execute(filter: ShareFilter): List<Share> {
        return repository.findByFilter(filter)
    }
}