package com.ninetynine.companyapi.share.usecase

import com.ninetynine.companyapi.share.domain.Share
import com.ninetynine.companyapi.share.domain.ShareRepository

class SaveListShareCmd(private val shareRepository: ShareRepository, private val share: List<Share>) {

    fun execute(){
        share.forEach {
            shareRepository.save(it)
        }
    }
}