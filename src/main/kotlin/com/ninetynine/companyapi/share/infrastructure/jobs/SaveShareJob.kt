package com.ninetynine.companyapi.share.infrastructure.jobs

import com.ninetynine.companyapi.share.domain.ShareRepository
import com.ninetynine.companyapi.share.usecase.SaveListShareCmd
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class SaveShareJob(
    @Qualifier("httpRepository") private val repository: ShareRepository,
    @Qualifier("jpaRepository") private val jpaRepository: ShareRepository
) {

    @Scheduled(fixedDelay = 10000)
    fun run(){
        val shares = repository.findAll()
        val saveCmd = SaveListShareCmd(jpaRepository, shares)
        saveCmd.execute()
    }
}