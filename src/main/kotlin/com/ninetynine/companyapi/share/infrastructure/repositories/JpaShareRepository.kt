package com.ninetynine.companyapi.share.infrastructure.repositories

import com.ninetynine.companyapi.share.domain.Share
import com.ninetynine.companyapi.share.domain.ShareFilter
import com.ninetynine.companyapi.share.domain.ShareRepository
import jakarta.persistence.EntityManager
import jakarta.persistence.criteria.Predicate
import jakarta.transaction.Transactional
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Primary
import org.springframework.stereotype.Repository
import java.util.*

@Primary
@Repository
@Qualifier("jpaRepository")
@Transactional
class JpaShareRepository(
    private val  entityManager: EntityManager
): ShareRepository {

    override fun save(share: Share) {
        entityManager.persist(share)
    }

    override fun findAll(): List<Share> {
       TODO("")
    }

    override fun findByFilter(filter: ShareFilter): List<Share> {
        //TODO verify nullability
        val criteriaBuilder = entityManager.criteriaBuilder
        val query = criteriaBuilder.createQuery(Share::class.java)
        val share = query.from(Share::class.java)

        val where = mutableListOf<Predicate>()

        where.add(criteriaBuilder.equal(share.get<Long>("company").get<Long>("id"), filter.companyId))
        where.add(criteriaBuilder.between(share.get("date"), filter.from, filter.to))

        return entityManager.createQuery(query).resultList
    }
}