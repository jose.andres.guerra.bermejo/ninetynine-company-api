package com.ninetynine.companyapi.share.infrastructure.repositories

import com.ninetynine.companyapi.company.domain.Company
import com.ninetynine.companyapi.share.domain.Share
import com.ninetynine.companyapi.share.domain.ShareFilter
import com.ninetynine.companyapi.share.domain.ShareRepository
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Repository
import java.time.Instant
import java.util.*

@Repository
@Qualifier("httpRepository")
class HttpShareRepository: ShareRepository {
    override fun save(share: Share) {

    }

    override fun findAll(): List<Share> {
        val share = Share(
            price = Random().nextDouble(),
            date =  Date.from(Instant.now()),
            company = Company(1, "ninetynine")
        )
        return listOf(
            share,
            share.copy(price = Random().nextDouble(), company = Company(id = 2, name = "ninetyeight")),
            share.copy(price = Random().nextDouble(), company = Company(id = 3, name = "ninetyseven"))
        )
    }

    override fun findByFilter(filter: ShareFilter): List<Share> {
        TODO("Not yet implemented")
    }
}