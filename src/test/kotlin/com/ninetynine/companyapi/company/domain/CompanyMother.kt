package com.ninetynine.companyapi.company.domain

object CompanyMother {

    fun of(
        id: Long = 1,
        name: String = "ninetynine"
    ): Company {
        return Company(id, name)
    }
}