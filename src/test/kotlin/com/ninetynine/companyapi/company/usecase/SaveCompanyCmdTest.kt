package com.ninetynine.companyapi.company.usecase

import com.ninetynine.companyapi.company.domain.CompanyMother
import com.ninetynine.companyapi.company.domain.CompanyRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test

class SaveCompanyCmdTest() {

    @Test
    fun `when save a company then use repository`(){
        val repository = mockk<CompanyRepository>()
        every {
            repository.save(COMPANY)
        } returns Unit

        SaveCompanyCmd(repository).execute(COMPANY)

        verify(exactly = 1) { repository.save(COMPANY) }
    }

    companion object {
        val COMPANY = CompanyMother.of()
    }
}