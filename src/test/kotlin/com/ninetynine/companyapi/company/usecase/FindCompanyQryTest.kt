package com.ninetynine.companyapi.company.usecase

import com.ninetynine.companyapi.company.domain.Company
import com.ninetynine.companyapi.company.domain.CompanyMother
import com.ninetynine.companyapi.company.domain.CompanyRepository
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class FindCompanyQryTest {

    @Test
    fun `when ask all companies then get a company list`(){
        val repository = mockk<CompanyRepository>()
        every { repository.findAll() } returns listOf(COMPANY)

        val sut = FindCompanyQry(repository).execute()

        assertTrue(sut.isNotEmpty())
    }

    companion object {
        val COMPANY = CompanyMother.of()
    }

}