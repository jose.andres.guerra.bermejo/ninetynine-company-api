package com.ninetynine.companyapi.share.usecase

import com.ninetynine.companyapi.company.domain.CompanyMother
import com.ninetynine.companyapi.share.domain.Share
import com.ninetynine.companyapi.share.domain.ShareRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test
import java.time.Instant
import java.util.*

class SaveListShareCmdTest {

    @Test
    fun `when save a share then call to repository`(){
        val repository = mockk<ShareRepository>()
        every { repository.save(SHARE) } returns Unit

        SaveListShareCmd(repository, listOf(SHARE)).execute()

        verify(exactly = 1) { repository.save(SHARE) }
    }

    @Test
    fun `when save a share without a saved company then create a company`(){
        //TODO verify if a company is created
    }

    companion object {
        val SHARE = Share(null, 1.1, Date.from(Instant.now()), CompanyMother.of())
    }
}