package com.ninetynine.companyapi.share.usecase

import com.ninetynine.companyapi.company.domain.CompanyMother
import com.ninetynine.companyapi.share.domain.Share
import com.ninetynine.companyapi.share.domain.ShareFilter
import com.ninetynine.companyapi.share.domain.ShareRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.time.Instant
import java.util.*

class FindByFilterShareQryTest {

    @Test
    fun `when send filter get filtered shares`(){
        val repository = mockk<ShareRepository>()
        every { repository.findByFilter(FILTER) } returns listOf(SHARE)

        val sut = FindByFilterShareQry(repository).execute(FILTER)

        verify { repository.findByFilter(FILTER) }
        assertEquals(listOf(SHARE), sut)
    }

    companion object {
        val FILTER = ShareFilter(companyId = 1, Date.from(Instant.now()), Date.from(Instant.now().plusMillis(1000)))
        val SHARE = Share(1, 1.0, Date.from(Instant.now()), CompanyMother.of())
    }
}